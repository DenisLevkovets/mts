const path = require('path');
const VueLoaderPlugin = require('vue-loader/lib/plugin');
const VuetifyLoaderPlugin = require('vuetify-loader/lib/plugin');


module.exports = {
    mode: 'development',
    module: {
        rules: [
            {
                parser: {
                    amd: false
                }
            },
            {
                test: /\.js$/,
                use: 'babel-loader'
            },
            {
                test: /\.vue$/,
                use: 'vue-loader'
            },
            {
                test: /\.css$/,
                use: ['vue-style-loader', 'css-loader']
            },
            {
                test: /\.styl$/,
                use: [
                    {
                        loader: "style-loader" // creates style nodes from JS strings
                    },
                    {
                        loader: "css-loader" // translates CSS into CommonJS
                    },
                    {
                        loader: "stylus-loader" // compiles Stylus to CSS
                    }
                ]
            },
            {
                test: /\.(jpe?g|png|gif|svg)$/i,
                use: "file-loader?name=/static/images/[name].[ext]"
            }
        ]
    },
    resolve: {
        extensions: ['*', '.js', '.json', '.vue', '.css', '.png', '.svg'],
        alias: {
            vue: 'vue/dist/vue.js',
        }
    },
    plugins: [
        new VueLoaderPlugin(),
        new VuetifyLoaderPlugin(),
    ],
    entry: {
        client: './src/main.js',
    },
    output: {
        path: path.resolve(__dirname, 'static/js'),
        filename: 'main-bundle.js'
    }
};
