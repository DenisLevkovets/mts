const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
var multer = require('multer');
var fs = require('fs');
var JSZip = require("jszip");
var storage = multer.diskStorage(
    {
        destination: './uploads/',
        filename: function (req, file, cb) {
            //req.body is empty...
            //How could I get the new_file_name property sent from client here?
            cb(null, file.originalname);
        }
    }
);

var upload = multer({storage: storage});
var convertapi = require('convertapi')('iq7mpOGK1HCKbuJK');

const app = express();
const port = 3210;

app.use(bodyParser.json());
app.use(cors());
app.post('/check', upload.any(), (req, res)=> {

    let files = req.files;
    var zip = new JSZip();
    let count = 0;
    for (let i = 0; i < files.length; i++) {
        convertapi.convert(req.body.to, {
            File: './uploads/' + files[i].filename
        }, req.body.from[i]).then(function (result) {
            result.saveFiles('./uploads/').then(function () {

                let newFileName = files[i].filename.toString().substring(0, files[i].filename.toString().length - req.body.from[i].toString().length - 1) + '.' + req.body.to;
                zip.file(newFileName, './uploads/' + newFileName);

                count++;
                if (files.length !== 1) {
                    if (count === files.length) {
                        zip.generateNodeStream({type: 'nodebuffer', streamFiles: true})
                            .pipe(fs.createWriteStream('./uploads/out.zip'))
                            .on('finish', function () {
                                res.sendFile(__dirname + '/uploads/out.zip')
                            });
                    }
                    fs.unlinkSync('./uploads/' + newFileName);
                }
                else {
                    res.sendFile(__dirname + '/uploads/' + newFileName)
                }
                fs.unlinkSync('./uploads/' + files[i].filename);

            });


        });

    }


});


app.listen(port, () => {
    console.log('Server works on port 3210')
});
