from flask import Flask, render_template

app = Flask('prof')


@app.route('/')
def client():
    return render_template('index.html')




@app.context_processor
def now_ts():
    import datetime
    return {'now_ts': str(datetime.datetime.now().timestamp())}
