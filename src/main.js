import Vue from 'vue'
import Vuetify from 'vuetify/lib'
import 'vuetify/dist/vuetify.min.css'
import App from './App'

Vue.use(Vuetify);

new Vue({
  el: '#app',
    ...App
});